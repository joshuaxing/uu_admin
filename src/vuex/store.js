/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-30 23:05:41
 * @LastEditTime: 2019-08-19 14:56:27
 * @LastEditors: Please set LastEditors
 */
import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);
let sidebarHotel = [{
    icon: "icon-jiudian1",
    url: "",
    name: "酒店中心",
    id: "1",
    children: [{
      id: "2",
      icon: "",
      url: "/hotel/base",
      name: "基本信息",
      children: []
    }]
  },
  {
    icon: "icon-yanhuiting1",
    url: "",
    name: "宴会厅管理",
    id: "3",
    children: [{
      id: "4",
      icon: "",
      url: "/hotel/yht",
      name: "宴会厅列表",
      children: []
    }]
  },
  {
    icon: "icon-huodong4",
    url: "",
    name: "活动管理",
    id: "5",
    children: [{
      id: "6",
      icon: "",
      url: "/hotel/activity",
      name: "活动列表",
      children: []
    }]
  },
  {
    icon: "icon-navicon-cprkd",
    url: "",
    name: "菜单管理",
    id: "7",
    children: [{
      id: "8",
      icon: "",
      url: "/hotel/menu",
      name: "菜单列表",
      children: []
    }, {
      id: "9",
      icon: "",
      url: "/hotel/caipin",
      name: "菜品列表",
      children: []
    }]
  },
  {
    icon: "icon-caidan-2",
    url: "",
    name: "预约管理",
    id: "10",
    children: [{
      icon: "",
      url: "/hotel/order",
      name: "预约大厅",
      id: "11",
      children: []
    }, {
      icon: "",
      url: "/hotel/myorder",
      name: "我的预约",
      id: "12",
      children: []
    }]
  },
  {
    icon: "icon-shenhe1",
    url: "",
    name: "我的审核",
    id: "13",
    children: [{
      id: "14",
      icon: "",
      url: "/hotel/edit",
      name: "审核列表",
      children: []
    }]
  },
]
let sidebarMerchant = [{
    icon: "icon-dianpu",
    url: "",
    name: "婚庆店中心",
    id: "1",
    children: [{
      id: "2",
      icon: "",
      url: "/merchant/base",
      name: "基本信息",
      children: []
    }]
  },
  {
    icon: "icon-chanpin4",
    url: "",
    name: "产品管理",
    id: "3",
    children: [{
      id: "4",
      icon: "",
      url: "/merchant/product",
      name: "产品列表",
      children: []
    },{
      id: "5",
      icon: "",
      url: "/merchant/stylist",
      name: "设计师列表",
      children: []
    }]
  },
  {
    icon: "icon-anli3",
    url: "",
    name: "案例管理",
    id: "6",
    children: [{
      id: "7",
      icon: "",
      url: "/merchant/case",
      name: "案例列表",
      children: []
    }]
  },
  {
    icon: "icon-baogaogongdan",
    url: "",
    name: "订单管理",
    id: "8",
    children: [{
      id: "9",
      icon: "",
      url: "/merchant/order",
      name: "订单列表",
      children: []
    }]
  },
  {
    icon: "icon-siyi1",
    url: "",
    name: "司仪管理",
    id: "10",
    children: [{
      id: "11",
      icon: "",
      url: "/merchant/siyi",
      name: "司仪列表",
      children: []
    }]
  },
  {
    icon: "icon-sheying",
    url: "",
    name: "摄影管理",
    id: "12",
    children: [{
      id: "13",
      icon: "",
      url: "/merchant/sheying",
      name: "摄影列表",
      children: []
    }]
  },
  {
    icon: "icon-huazhuang",
    url: "",
    name: "跟妆管理",
    id: "14",
    children: [{
      id: "15",
      icon: "",
      url: "/merchant/genzhuang",
      name: "跟妆列表",
      children: []
    }]
  },
  {
    icon: "icon-huodong4",
    url: "",
    name: "营销管理",
    id: "16",
    children: [{
      id: "17",
      icon: "",
      url: "/merchant/coupon",
      name: "营销活动",
      children: []
    }]
  },
  {
    icon: "icon-anli2",
    url: "",
    name: "预约管理",
    id: "18",
    children: [{
      icon: "",
      url: "/merchant/merchantorder",
      name: "预约大厅",
      id: "19",
      children: []
    }, {
      icon: "",
      url: "/merchant/mymerchantorder",
      name: "我的预约",
      id: "20",
      children: []
    }]
  },
  {
    icon: "icon-shenhe1",
    url: "",
    name: "我的审核",
    id: "21",
    children: [{
      id: "22",
      icon: "",
      url: "/merchant/edit",
      name: "审核列表",
      children: []
    }]
  }
]
let sidebarCar = [{
    icon: "icon-dianpu",
    url: "/car/base",
    name: "婚车中心",
    id: "1",
    children: [{
      icon: "",
      url: "/car/base",
      name: "基本信息",
      id: "2",
      children:[]
    }]
  },
  {
    icon: "icon-hunchezulin",
    url: "",
    name: "婚车管理",
    id: "3",
    children: [{
      icon: "",
      url: "/car/huiche",
      name: "婚车列表",
      id: "4",
      children: []
    }]
  },
  {
    icon: "icon-icon",
    url: "",
    name: "预约管理",
    id: "4",
    children: [{
      icon: "",
      url: "/car/order",
      name: "预约大厅",
      id: "5",
      children: []
    }, {
      icon: "",
      url: "/car/myorder",
      name: "我的预约",
      id: "6",
      children: []
    }]
  },
  {
    icon: "icon-shenhe1",
    url: "",
    name: "我的审核",
    id: "7",
    children: [{
      id: "8",
      icon: "",
      url: "/car/edit",
      name: "审核列表",
      children: []
    }]
  },
]
let sideDesign = [{
  icon: "icon-dianpu",
  url: "",
  name: "设计师基本信息",
  id: "1",
  children: [{
    id: "2",
    icon: "",
    url: "/design/index",
    name: "基本信息",
    children: []
  }]
},{
  icon: "icon-chanpin4",
  url: "",
  name: "作品管理",
  id: "3",
  children: [{
    id: "4",
    icon: "",
    url: "/design/work",
    name: "作品列表",
    children: []
  }]
},{
  icon: "icon-baogaogongdan",
  url: "",
  name: "收益管理",
  id: "5",
  children: [{
    id: "6",
    icon: "",
    url: "/design/earnings",
    name: "收益列表",
    children: []
  }]
}]
let store = new Vuex.Store({
  state: {
    uulogin: {
      name: '',
      account: '',
      id: 0,
      type: 0,
      logoName: '',
      province: '',
      city: '',
      area: ''
    },
    tagsList: [],
    collapse: false,
    sidebarMenus: []
  },
  getters: {

  },
  mutations: {
    uulogin(state, payload) {
      let data = payload.data;
      let logoName = ''
      if (data.type === 1) {
        //酒店
        logoName = '酒店管理系统';
        state.sidebarMenus = sidebarHotel
        // console.log('酒店')
      } else if (data.type === 2) {
        //商家
        logoName = '商户管理系统';
        state.sidebarMenus = sidebarMerchant
        // console.log('商家')
      } else if (data.type === 3){
        //婚车
        logoName = '婚车管理系统';
        state.sidebarMenus = sidebarCar
        // console.log('婚车')
      } else {
        logoName = '设计师管理系统';
        state.sidebarMenus = sideDesign
      }
      state.uulogin = {
        ...data,
        logoName: logoName
      };
    },
    tags(state, payload) {
      const data = payload.value;
      let arr = [];
      for (let i = 0, len = data.length; i < len; i++) {
        data[i].name && arr.push(data[i].name);
      }
      state.tagsList = arr;
    }
  }
})

export default store
