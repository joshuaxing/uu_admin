/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-30 23:05:41
 * @LastEditTime: 2019-08-19 11:26:14
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store';
import http from '@/axios/http';
Vue.use(Router);
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/regist',
      name: 'regist',
      component: resolve => require(['../components/admin/Regist/index.vue'], resolve)
    },
    {
      path: '/login',
      name: 'login',
      component: resolve => require(['../components/admin/Regist/login.vue'], resolve)
    },
    {
      path: '/forget',
      name: 'forget',
      component: resolve => require(['../components/admin/Regist/forget.vue'], resolve)
    },
    {
      path: '/hotel',
      name: 'hotel',
      component: resolve => require(['../components/admin/Home/index.vue'], resolve),
      meta: {
        requiresAuthValue: 1
      },
      redirect: '/hotel/base',
      children: [
        {
          path: '/hotel/base',
          name: 'hotelbase',
          component: resolve => require(['../components/admin/Hotel/base.vue'], resolve),
          meta: {
            title: '基本信息',
            requiresAuthValue: 1
          }
        },
        {
          path: '/hotel/yht',
          name: 'hotelyht',
          component: resolve => require(['../components/admin/Hotel/yht.vue'], resolve),
          meta: {
            title: '宴会厅列表',
            requiresAuthValue: 2
          }
        },
        {
          path: '/hotel/activity',
          name: 'hotelactivity',
          component: resolve => require(['../components/admin/Hotel/activity.vue'], resolve),
          meta: {
            title: '活动列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/hotel/menu',
          name: 'hotelmenu',
          component: resolve => require(['../components/admin/Hotel/menu.vue'], resolve),
          meta: {
            title: '菜单列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/hotel/caipin',
          name: 'hotelcaipin',
          component: resolve => require(['../components/admin/Hotel/caipin.vue'], resolve),
          meta: {
            title: '菜品列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/hotel/order',
          name: 'hotelorder',
          component: resolve => require(['../components/admin/Hotel/hotelorder.vue'], resolve),
          meta: {
            title: '预约大厅',
            requiresAuthValue: 2
          },
        },
        {
           path: '/hotel/myorder',
           name: 'myhotelorder',
           component: resolve => require(['../components/admin/Hotel/myhotelorder.vue'], resolve),
           meta: {
             title: '我的预约',
             requiresAuthValue: 2
           },
         },
        {
          path: '/hotel/edit',
          name: 'hoteledit',
          component: resolve => require(['../components/admin/Hotel/edit.vue'], resolve),
          meta: {
            title: '审核列表',
            requiresAuthValue: 2
          },
        }
      ]
    },
    {
      path: '/merchant',
      name: 'merchant',
      component: resolve => require(['../components/admin/Home/index.vue'], resolve),
      meta: {
        requiresAuthValue: 1
      },
      redirect: '/merchant/base',
      children: [{
          path: '/merchant/base',
          name: 'merchantbase',
          component: resolve => require(['../components/admin/Merchant/base.vue'], resolve),
          meta: {
            title: '基本信息',
            requiresAuthValue: 1
          }
        },
        {
          path: '/merchant/stylist',
          name: 'merchantstylist',
          component: resolve => require(['../components/admin/Merchant/stylist.vue'], resolve),
          meta: {
            title: '设计师列表',
            requiresAuthValue: 2
          }
        },
        {
          path: '/merchant/product',
          name: 'merchantproduct',
          component: resolve => require(['../components/admin/Merchant/product.vue'], resolve),
          meta: {
            title: '产品列表',
            requiresAuthValue: 2
          }
        },
        {
          path: '/merchant/case',
          name: 'merchantcase',
          component: resolve => require(['../components/admin/Merchant/case.vue'], resolve),
          meta: {
            title: '案例列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/order',
          name: 'merchantorderlist',
          component: resolve => require(['../components/admin/Merchant/order.vue'], resolve),
          meta: {
            title: '订单列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/order/add',
          name: 'merchantorderadd',
          component: resolve => require(['../components/admin/Merchant/orderadd.vue'], resolve),
          meta: {
            title: '订单新增',
            requiresAuthValue: 2
          },
        },
        {
          path: '/order/edit',
          name: 'merchantorderedit',
          component: resolve => require(['../components/admin/Merchant/orderedit.vue'], resolve),
          meta: {
            title: '订单编辑',
            requiresAuthValue: 2
          },
        },
         {
           path: '/order/cat',
           name: 'merchantordercat',
           component: resolve => require(['../components/admin/Merchant/ordercat.vue'], resolve),
           meta: {
             title: '订单详情',
             requiresAuthValue: 2
           },
         },
        {
          path: '/merchant/siyi',
          name: 'merchantsiyi',
          component: resolve => require(['../components/admin/Merchant/siyi.vue'], resolve),
          meta: {
            title: '司仪列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/sheying',
          name: 'merchantsheying',
          component: resolve => require(['../components/admin/Merchant/sheying.vue'], resolve),
          meta: {
            title: '摄影列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/genzhuang',
          name: 'merchantgenzhuang',
          component: resolve => require(['../components/admin/Merchant/genzhuang.vue'], resolve),
          meta: {
            title: '跟妆列表',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/coupon',
          name: 'merchantcoupon',
          component: resolve => require(['../components/admin/Merchant/coupon.vue'], resolve),
          meta: {
            title: '营销活动',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/merchantorder',
          name: 'merchantorder',
          component: resolve => require(['../components/admin/Merchant/merchantorder.vue'], resolve),
          meta: {
            title: '预约大厅',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/mymerchantorder',
          name: 'mymerchantorder',
          component: resolve => require(['../components/admin/Merchant/mymerchantorder.vue'], resolve),
          meta: {
            title: '我的预约',
            requiresAuthValue: 2
          },
        },
        {
          path: '/merchant/edit',
          name: 'merchantedit',
          component: resolve => require(['../components/admin/Merchant/edit.vue'], resolve),
          meta: {
            title: '审核列表',
            requiresAuthValue: 2
          },
        }
      ]
    },
    {
      path: '/car',
      name: 'car',
      component: resolve => require(['../components/admin/Home/index.vue'], resolve),
      meta: {
        requiresAuthValue: 1
      },
      redirect: '/car/base',
      children: [{
            path: '/car/base',
            name: 'carbase',
            component: resolve => require(['../components/admin/Car/base.vue'], resolve),
            meta: {
              title: '基本信息',
              requiresAuthValue: 1
            }
          },{
          path: '/car/huiche',
          name: 'carhuiche',
          component: resolve => require(['../components/admin/Car/huiche.vue'], resolve),
          meta: {
            title: '婚车管理',
            requiresAuthValue: 2
          }
        }, {
          path: '/car/order',
          name: 'carorder',
          component: resolve => require(['../components/admin/Car/carorder.vue'], resolve),
          meta: {
            title: '预约大厅',
            requiresAuthValue: 2
          }
        }, {
          path: '/car/myorder',
          name: 'mycarorder',
          component: resolve => require(['../components/admin/Car/mycarorder.vue'], resolve),
          meta: {
            title: '我的预约',
            requiresAuthValue: 2
          }
        },{
          path: '/car/edit',
          name: 'caredit',
          component: resolve => require(['../components/admin/Car/edit.vue'], resolve),
          meta: {
            title: '审核列表',
            requiresAuthValue: 2
          },
        }]
    },
    {
      path: '/design',
      name: 'design',
      component: resolve => require(['../components/admin/Home/index.vue'], resolve),
      meta: {
        requiresAuth: 1
      },
      redirect: '/design/index',
      children: [{
            path: '/design/index',
            name: 'designindex',
            component: resolve => require(['../components/admin/Design/design.vue'], resolve),
            meta: {
              title: '基本信息',
              requiresAuthValue: 1
            }
          },{
          path: '/design/work',
          name: 'designwork',
          component: resolve => require(['../components/admin/Design/works.vue'], resolve),
          meta: {
            title: '作品管理',
            requiresAuthValue: 1
          }
        }, {
          path: '/design/earnings',
          name: 'designearnings',
          component: resolve => require(['../components/admin/Design/earnings.vue'], resolve),
          meta: {
            title: '收益管理',
            requiresAuthValue: 1
          }
        }]
    },
    {
      path: '/nopower',
      name: 'nopower',
      component: resolve => require(['../components/admin/Regist/nopower.vue'], resolve)
    },
    {
      path: '*',
      redirect: '/404'
    }, {
      path: '/404',
      component: resolve => require(['../components/page/404.vue'], resolve)
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (sessionStorage.getItem('uulogin')) {
    store.commit({
      type: 'uulogin',
      data: JSON.parse(sessionStorage.getItem('uulogin'))
    })
  }
 
  if (to.matched.some(record => record.meta.requiresAuthValue)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    console.log('beforeEach')
    let data = store.state.uulogin;
    console.log(data)
     //登录管理后台
    if (data.id) {
      // console.log(to.matched)
    if(to.matched.some(record => record.meta.requiresAuthValue === 2)) {
        if (data.backupstatus === 2) {
          next()
        } else {
          next({
            path: '/nopower'
          })
        }
      } else {
        next()
      }
    } else if (to.matched.some(record => record.meta.requiresAuthValue === 1)) {
      next()
    }  else {
      next({
        path: '/login'
      })
    }
  } else {
    next() // 确保一定要调用 next()
  }
})
router.beforeResolve((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuthValue)) {
    http.post('/hotel/getmysession.do')
    .then((result) => {
      if (result.status === 0) {
        const uulogin = {
          name: '',
          account: '',
          id: 0,
          type: 0,
          logoName: '',
          province: '',
          city: '',
          area: ''
        }
        store.commit({
          type: 'uulogin',
          data: uulogin
        })
        sessionStorage.removeItem("uulogin");
        next({
          path: '/login'
        })
      } else {
        next()
      }
    })
  } else {
    next()
  }
    
})
export default router
