<!--
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-02 10:26:50
 * @LastEditTime: 2019-09-02 10:26:50
 * @LastEditors: your name
 -->
# ES6
  ### Babel 转码器
  1. 定义：Babel 是一个广泛使用的 ES6 转码器，可以将 ES6 代码转为 ES5 代码，从而在现有环境执行

  2. 配置文件.babelrc
    ```
    npm install --save-dev @babel/preset-env
    npm install --save-dev @babel/preset-react
    {
      "presets": [
        "@babel/env",
        "@babel/preset-react"
      ], //转码规则
      "plugins": [] //插件
    }
    ```

  3. 命令行转码
    ```
    npm install --save-dev @babel/cli
    # 转码结果输出到标准输出
    $ npx babel example.js

    # 转码结果写入一个文件
    # --out-file 或 -o 参数指定输出文件
    $ npx babel example.js --out-file compiled.js
    # 或者
    $ npx babel example.js -o compiled.js

    # 整个目录转码
    # --out-dir 或 -d 参数指定输出目录
    $ npx babel src --out-dir lib
    # 或者
    $ npx babel src -d lib

    # -s 参数生成source map文件
    $ npx babel src -d lib -s
    ```

  ### 解构赋值
  1. 数组结构赋值
   ```
   //模式匹配
   let [a, b, c] = [1, 2, 3] => let a = 1, let b = 2, let c = 3
   ```
  2. 对象的解构赋值
   ```

   ```
